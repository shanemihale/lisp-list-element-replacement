;;; repl.cl
;;; Shane Hale (smh1931@cs.rit.edu)
;;;
;;; Description:
;;;  Examines a list, and performs a find replace operation for the supplied
;;;  values

;;;
;;;  repl
;;;  Description:
;;;   Returns a new list, with the requested values replaced
;;;  Arguments:
;;;   find - the value to find
;;;   replace - the value to replace the find value with
;;;   lst - the list to perform the find replace operation
;;;  Returns:
;;;   new list with the replaced values, all other items remain the same
;;;
(defun repl (find replace lst)
    (cond
        ;base cases
        ((not (listp lst)) NIL)
        ((null lst) NIL)

        ;call the helper function to perform the find replace operation
        (T (helper find replace lst '()))
    )
)

;;;
;;; helper
;;; Description:
;;;  Iterates through the items in the list, and replaces them in the new list
;;;  if necessary
;;; Arguments:
;;;  find - the item to find
;;;  replace - the item to replace the find value with
;;;  lst - the original list
;;;  newLst - the new list that will contain the replaced items
;;; Returns:
;;;  new list with the find items replaced, the other items will remain
;;;  the same
;;;
(defun helper (find replace lst newLst)
    (cond
        ;base cases
        ((not (listp lst)) NIL)
        ((null lst) newLst)

        ;if the current item is a list, follow it recursively and call helper
        ;with it's values
        ((and (replaceListContains lst find) (listp (car lst)))
         (helper find replace (cdr lst)
          (append newLst (cons (helper find replace (car lst) '()) '()) '())
         )
        )

        ;if the find value is in the list, append the replace value to the
        ;new list
        ((and (replaceListContains lst find) (listp find))
         (append newLst replace)
        )
        
        ;call helper with the rest of the list
        ((replaceListContains lst find) 
         (helper find replace (cdr lst) 
          (append newLst (cons replace '()))
         )
        )
        
        ;call helper as a last resort
        (T (helper find replace (cdr lst) (append newLst (cons (car lst) '()))))
    )
)

;;;
;;; replaceListContains
;;; Description:
;;;  Identifies if the list contains the value to be replaced
;;; Arguments:
;;;  lst - the original list
;;;  item - the item to be found and replaced
;;; Returns:
;;;  T is the item exists in the lst
;;;  NIL if the item doesn't exist in the lst
;;;
(defun replaceListContains(lst item)
    (cond
        ;base cases
        ((null lst) NIL)
        ((and (listp item) (equal item lst)) T)
        ((equal (car lst) item) T)
        
        ;call replaceListContains for each sub item in lst
        ((listp (car lst)) (replaceListContains (car lst) item))
        (T NIL)
    )
)
